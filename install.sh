#!/bin/bash
if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$HOME/.dotfiles
    mv $HOME/.gitconfig $HOME/.gitconfig.bak
fi

ln -s $DOTFILE_PATH/.gitconfig $HOME/.gitconfig
ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases

# install bun and add it to path
if [ ! -d "$HOME/.local/bin" ]; then
    mkdir -p $HOME/.local/bin
fi

# curl -fsSL https://bun.sh/install | bash
npm -g install bun@1.0 # i need this in order to install the right version of bun
